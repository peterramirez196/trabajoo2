package facci.pm.ta2.poo.pra1;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.FindCallback;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6


        String id = getIntent().getStringExtra("object_id");




        DataQuery query = DataQuery.get("item");//Estamos invocando a nueestro DataQuery
        query.getInBackground(id , new GetCallback<DataObject>() {// estamos invoncando a nuestra  al a funcion getInBackground que sta en query y le
            // estamos enviando un parametro

            @Override
            public void done(DataObject object, DataException e) {

                //ESTAMOS DECLARANDO UN TEXTVIEW  Y LE ESTAMOS RELACIONANDO CPN "title"
                TextView title = (TextView)findViewById(R.id.title);
                title.setText((String) object.get("name"));
                //ESTAMOS DECLARANDO UN TEXTVIEW CON EL NOMBRE DE "textvies"  Y LE ESTAMOS DICIENDO QUE ESTA RELACIONADO  CON NUESTRO TEXTVIEW Pdescripcion
                TextView textView =(TextView)findViewById(R.id.descripcion);
                textView.setMovementMethod(new ScrollingMovementMethod());
                textView.setText((String) object.get("description"));
                //ESTAMOS DECLARANDO UN IMAGEVIEW CON EL NOMBRE DE "thumbnail"  Y LE ESTAMOS DICIENDO QUe ESTA RELACIONADO  CON NUESTRA ImageView thumbnail
                ImageView thumbnail=  (ImageView)findViewById(R.id.thumbnail);
                //ESTAMOS DICIENDO QUE EN "thumbnail" SE MOSTRARA LO QUE TENEMOS EN NUESTRO OBJETO EN LA PROPIEDAD "image"
                thumbnail.setImageBitmap((Bitmap) object.get("image"));
                  //ESTAMOS DECLARANDO UN TEXTVIEW CON EL NOMBRE DE "price"  Y LE ESTAMOS DICIENDO QUE ESTA RELACIONADO  CON NUESTRO TEXTVIEW PRICE
                TextView price = (TextView)findViewById(R.id.price);
                price.setText((String) object.get("price")); //ESTAMOS DICIENDO QUE EN "price" SE MOSTRARA LO QUE TENEMOS EN NUESTRO OBJETO EN LA PROPIEDAD "price"



            }
        });
        // FIN - CODE6

    }

}
